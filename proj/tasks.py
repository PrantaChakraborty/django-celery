from celery import shared_task
from django.core.mail import BadHeaderError, send_mail
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect


@shared_task
def add(x, y):
    return x+y


@shared_task
def send_email(subject, message, from_email):
    if subject and message and from_email:
        try:
            send_mail(subject, message, from_email, ['cbpranta@gmail.com'])
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        return HttpResponseRedirect('/')
    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        return HttpResponse('Make sure all fields are entered and valid.')

import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'celery_task.settings')

app = Celery('celery_task')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()


#  to run celery ( celery -A project_name(celery_task) -l info
@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')